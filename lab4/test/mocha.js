const expect  = require('chai').expect;
const config = require('../config');

describe('Mocha', () => {
    describe('Check adding products', () => {
        it('To cart', async () => {
            const data = {
                productPageUrl: config.websiteUrl + 'zinzi-classy-horloge-34mm-donkerrode-wijzerplaat-rosegoudkleurige-stalen-kast-en-bicolor-band-datum-ziw1038',
                cartPageUrl: config.websiteUrl + 'checkout/cart/',
                productQuantity: '10'
            };

            const webdriver = require('selenium-webdriver'),
                By = webdriver.By,
                until = webdriver.until;

            const driver = new webdriver.Builder()
                .forBrowser('chrome')
                .build();

            await driver.get(data.productPageUrl);

            let quantityInput = await driver.findElement(By.id('qty'));
            await quantityInput.clear();
            await quantityInput.sendKeys(data.productQuantity);

            let addProductButton = await driver.findElement(By.id('product-addtocart-button'));
            await addProductButton.click();

            await driver.sleep(config.pageLoadTime);

            let counterElement = (await driver.findElements(By.className('counter-number')))[0];
            let counterValue = await counterElement.getText();

            let urlString = await driver.getCurrentUrl();

            await driver.quit();

            expect(urlString).to.equal(data.cartPageUrl);
            expect(counterValue).to.equal(data.productQuantity);
        });
    });

    describe('Check address editing', () => {
        it('With invalid data', async () => {
            const data = {
                loginPageUrl: config.websiteUrl + 'customer/account/login/',
                addressBookUrl: config.websiteUrl + 'customer/address/index/',
                addressEditPageUrl: config.websiteUrl + 'customer/address/edit/'
            };

            const webdriver = require('selenium-webdriver'),
                By = webdriver.By,
                until = webdriver.until;

            const driver = new webdriver.Builder()
                .forBrowser('chrome')
                .build();

            await driver.get(data.loginPageUrl);

            let emailInput = await driver.findElement(By.id('email'));
            emailInput.sendKeys(config.email);

            let passwordInput = await driver.findElement(By.id('pass'));
            passwordInput.sendKeys(config.password);

            let loginButton = (await driver.findElements(By.className('action login primary')))[0];
            await loginButton.click();

            await driver.get(data.addressBookUrl);

            let addressSection = (await driver.findElements(By.className('box box-address-billing')))[0];
            let editAddressButton = (await addressSection.findElements(By.className('action edit')))[0];
            await editAddressButton.click();

            await driver.sleep(config.pageLoadTime);

            let phoneNumberInput = await driver.findElement(By.id('telephone'));
            await phoneNumberInput.clear();

            let saveButton = (await driver.findElements(By.className('action save primary')))[0];
            await saveButton.click();

            let urlString = await driver.getCurrentUrl();

            await driver.quit();

            expect(urlString.includes(data.addressEditPageUrl)).to.equal(true);
        });

        it('With valid data', async () => {
            const data = {
                loginPageUrl: config.websiteUrl + 'customer/account/login/',
                addressBookUrl: config.websiteUrl + 'customer/address/index/',
                phoneNumber: '0987654321'
            };

            const webdriver = require('selenium-webdriver'),
                By = webdriver.By,
                until = webdriver.until;

            const driver = new webdriver.Builder()
                .forBrowser('chrome')
                .build();

            await driver.get(data.loginPageUrl);

            let emailInput = await driver.findElement(By.id('email'));
            emailInput.sendKeys(config.email);

            let passwordInput = await driver.findElement(By.id('pass'));
            passwordInput.sendKeys(config.password);

            let loginButton = (await driver.findElements(By.className('action login primary')))[0];
            await loginButton.click();

            await driver.get(data.addressBookUrl);

            let addressSection = (await driver.findElements(By.className('box box-address-billing')))[0];
            let editAddressButton = (await addressSection.findElements(By.className('action edit')))[0];
            await editAddressButton.click();

            await driver.sleep(config.pageLoadTime);

            let phoneNumberInput = await driver.findElement(By.id('telephone'));
            await phoneNumberInput.clear();
            await phoneNumberInput.sendKeys(data.phoneNumber);

            let saveButton = (await driver.findElements(By.className('action save primary')))[0];
            await saveButton.click();

            let urlString = await driver.getCurrentUrl();

            await driver.quit();

            expect(urlString).to.equal(data.addressBookUrl);
        });
    });

    describe('Check login', () => {
        it('With invalid credentials', async () => {
            const data = {
                loginPageUrl: config.websiteUrl + 'customer/account/login/',
                fakePassword: 'fakepass'
            };

            const webdriver = require('selenium-webdriver'),
                By = webdriver.By,
                until = webdriver.until;

            const driver = new webdriver.Builder()
                .forBrowser('chrome')
                .build();

            await driver.get(data.loginPageUrl);

            let emailInput = await driver.findElement(By.id('email'));
            emailInput.sendKeys(config.email);

            let passwordInput = await driver.findElement(By.id('pass'));
            passwordInput.sendKeys(data.fakePassword);

            let loginButton = (await driver.findElements(By.className('action login primary')))[0];
            await loginButton.click();

            let urlString = await driver.getCurrentUrl();

            await driver.quit();

            expect(urlString).to.equal(data.loginPageUrl);
        });

        it('With valid credentials', async () => {
            const data = {
                loginPageUrl: config.websiteUrl + 'customer/account/login/',
                accountPageUrl: config.websiteUrl + 'customer/account/'
            };

            const webdriver = require('selenium-webdriver'),
                By = webdriver.By,
                until = webdriver.until;

            const driver = new webdriver.Builder()
                .forBrowser('chrome')
                .build();

            await driver.get(data.loginPageUrl);

            let emailInput = await driver.findElement(By.id('email'));
            emailInput.sendKeys(config.email);

            let passwordInput = await driver.findElement(By.id('pass'));
            passwordInput.sendKeys(config.password);

            let loginButton = (await driver.findElements(By.className('action login primary')))[0];
            await loginButton.click();

            let urlString = await driver.getCurrentUrl();

            await driver.quit();

            expect(urlString).to.equal(data.accountPageUrl);
        });
    });

    describe('Check page', () => {
        it('Of logout', async () => {
            const data = {
                loginPageUrl: config.websiteUrl + 'customer/account/login/',
                logoutControllerUrl: config.websiteUrl + 'customer/account/logout/',
                logoutSuccessPageUrl: config.websiteUrl + 'customer/account/logoutSuccess/'
            };

            const webdriver = require('selenium-webdriver'),
                By = webdriver.By,
                until = webdriver.until;

            const driver = new webdriver.Builder()
                .forBrowser('chrome')
                .build();

            await driver.get(data.loginPageUrl);

            let emailInput = await driver.findElement(By.id('email'));
            emailInput.sendKeys(config.email);

            let passwordInput = await driver.findElement(By.id('pass'));
            passwordInput.sendKeys(config.password);

            let loginButton = (await driver.findElements(By.className('action login primary')))[0];
            await loginButton.click();

            await driver.get(data.logoutControllerUrl);

            let urlString = await driver.getCurrentUrl();

            await driver.quit();

            expect(urlString).to.equal(data.logoutSuccessPageUrl);
        });

        it('Of minicart', async () => {
            const data = {};

            const webdriver = require('selenium-webdriver'),
                By = webdriver.By,
                until = webdriver.until;

            const driver = new webdriver.Builder()
                .forBrowser('chrome')
                .build();

            await driver.get(config.websiteUrl);

            await driver.sleep(config.pageLoadTime * 2);

            let minicartButton = (await driver.findElements(By.className('action showcart')))[0];
            let minicartElement = await driver.findElement(By.id('ui-id-1'));

            await minicartButton.click();
            let isMinicartAppear = await minicartElement.isDisplayed();

            await minicartButton.click();
            let isMinicartDisappear = !(await minicartElement.isDisplayed());

            await driver.quit();

            expect(isMinicartAppear && isMinicartDisappear).to.equal(true);
        });

        it('Of search', async () => {
            const data = {
                searchText: 'Ring'
            };

            const webdriver = require('selenium-webdriver'),
                By = webdriver.By,
                until = webdriver.until;

            const driver = new webdriver.Builder()
                .forBrowser('chrome')
                .build();

            await driver.get(config.websiteUrl);

            let searchInput = await driver.findElement(By.id('search'));
            await searchInput.sendKeys(data.searchText);

            await driver.sleep(config.pageLoadTime);

            let searchButton = (await driver.findElements(By.className('action search')))[0];
            await searchButton.click();

            let urlString = await driver.getCurrentUrl();
            let url = new URL(urlString);
            let urlSearchText = url.searchParams.get('q');

            await driver.quit();

            expect(urlSearchText).to.equal(data.searchText);
        });
    });

    describe('Check removing products', () => {
        it('From cart', async () => {
            const data = {
                productPageUrl: config.websiteUrl + 'zinzi-classy-horloge-34mm-donkerrode-wijzerplaat-rosegoudkleurige-stalen-kast-en-bicolor-band-datum-ziw1038',
                cartPageUrl: config.websiteUrl + 'checkout/cart/',
                productQuantity: '0'
            };

            const webdriver = require('selenium-webdriver'),
                By = webdriver.By,
                until = webdriver.until;

            const driver = new webdriver.Builder()
                .forBrowser('chrome')
                .build();

            await driver.get(data.productPageUrl);

            let addProductButton = await driver.findElement(By.id('product-addtocart-button'));
            await addProductButton.click();

            await driver.sleep(config.pageLoadTime);

            let removeProductButton = (await driver.findElements(By.className('action action-delete')))[0];
            await removeProductButton.click();

            await driver.sleep(config.pageLoadTime);

            let counterElement = (await driver.findElements(By.className('counter-number')))[0];
            let counterValue = await counterElement.getText();

            let urlString = await driver.getCurrentUrl();

            await driver.quit();

            expect(urlString).to.equal(data.cartPageUrl);
            expect(counterValue).to.equal(data.productQuantity);
        });

        it('From minicart', async () => {
            const data = {
                productPageUrl: config.websiteUrl + 'zinzi-classy-horloge-34mm-donkerrode-wijzerplaat-rosegoudkleurige-stalen-kast-en-bicolor-band-datum-ziw1038',
                productQuantity: '0'
            };

            const webdriver = require('selenium-webdriver'),
                By = webdriver.By,
                until = webdriver.until;

            const driver = new webdriver.Builder()
                .forBrowser('chrome')
                .build();

            await driver.get(data.productPageUrl);

            let addProductButton = await driver.findElement(By.id('product-addtocart-button'));
            await addProductButton.click();

            await driver.sleep(config.pageLoadTime);

            let minicartButton = (await driver.findElements(By.className('action showcart')))[0];
            await minicartButton.click();

            let removeProductButton = (await driver.findElements(By.className('action delete')))[0];
            await removeProductButton.click();

            await driver.sleep(config.pageLoadTime);

            let confirmButton = (await driver.findElements(By.className('action-primary action-accept')))[0];
            await confirmButton.click();

            await driver.sleep(config.pageLoadTime);

            let counterElement = (await driver.findElements(By.className('counter-number')))[0];
            let counterValue = await counterElement.getText();

            await driver.quit();

            expect(counterValue).to.equal(data.productQuantity);
        });
    });
});
